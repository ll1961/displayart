class Art < ActiveRecord::Base
 
 validates_presence_of :name, :thumb, :image, :series, :sort_order
 validates_numericality_of :sort_order
 attr_protected

  def find_series_count(series_name)
    Art.find_by_sql("SELECT * FROM arts WHERE series = '#{series_name}'").count
  end
  
  def self.find_all_series()
#    Art.select('distinct(arts.series)')
#    Art.select('distinct(arts.series)').map{|a| a.series }
    Art.select('distinct series').order('series ASC').map{|a| a.series }
  end
end
