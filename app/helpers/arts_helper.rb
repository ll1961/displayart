module ArtsHelper

  def draw_grid(size, cols, i)
    
      rows = size/cols 
			f = [0, cols-1]
			l = [size-cols, size-1]
			
			case i
			when (f[0]..f[1])
			   rend = "top"
			when (l[0]..l[1])
			   rend = "bottom"
			else
			   rend = "middle"
			end		
		
			if i % cols == 0					
				rend << "left"						
			elsif i % cols == cols -1 
				rend << "right"
			else 
				rend << "middle"
			end	
    end

end
