class CreateArts < ActiveRecord::Migration
  def change
    create_table :arts do |t|
      t.string :name
      t.string :thumb
      t.string :image
      t.boolean :sold
      t.string :size
      t.boolean :prints_available
      t.string :meta_data

      t.timestamps
    end
  end
end
