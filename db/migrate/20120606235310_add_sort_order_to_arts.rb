class AddSortOrderToArts < ActiveRecord::Migration
  def change
    add_column :arts, :sort_order, :integer
  end
end
